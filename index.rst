=======================================
Welcome to ANSI Colors's documentation!
=======================================

.. sidebar:: Sphinx

   This page is the main page of the *documentation* 
   for my project, realised with `Sphinx <http://sphinx-doc.org>`_.

Welcome to the *documentation* for **ANSIColors-balises**, 
a *python 2* module to use *ANSI colours* in a terminal.
This project is currently in *version* |version|, *release* |release|.
Last update of this doc was made |today|.

The *main page* of this little project is 
`here on BitBucket <https://bitbucket.org/lbesson/ansi-colors>`_.

The project is now hosted on **Pypi**, 
so a quick overview and the last release can be downloaded 
`from PyPi <https://pypi.python.org/pypi/ANSIColors-balises>`_.

.. seealso::

   This is the main page of the doc for my project, 
   but there are other resources *on-line* :
   This project is hosted on **bitbucket**, 
   see `here lbesson/ansi-colors <https://bitbucket.org/lbesson/ansi-colors>`_.
   And it is also hosted `here on my google site  
   <https://sites.google.com/site/naereencorp/liste-des-projets/ansi-colors>`_.


.. _installation:
.. include:: INSTALL

------------------------------------------------------------------------------

Examples
--------

The function ``printc``
~~~~~~~~~~~~~~~~~~~~~~~

The main function of this module is ``printc``.
This function is like ``print("my string")`` or 
``sys.stdout.write("my other string")``.

For instance, a quick description of super heroes's costums 
can be done like this : ::

    >>> printc("<reset><white>Batman's costum is <black>black<white>, Aquaman's costum is <blue>blue<white> and <green>green<white>, and Superman's costum is <red>red<white> and <blue>blue<white> ...")
    Batman's costum is black, Aquaman's costum is blue and green, and Superman's costum is red and blue ...


*Sorry, but it is hard to embed colours in a Sphinx generated web pages.*

``python -m ANSIColors --help``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This show the help of the script, 
colored with the tools defined in the script :

.. image:: example1.png
   :scale: 85 %
   :align: center
   :alt: The help message of the script.


``python -m ANSIColors --test``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This show a complete test of all balises defined in the module :

.. image:: example2.png
   :scale: 85 %
   :align: center
   :alt: The canonical test of the script.

Documentation
-------------

And, a detailed description of every function is available 
on the documentation `of the module ANSIColors <ANSIColors.html>`_.

-----------------------------------------------------------------------------

Author:
-------

The main author of this little project is **Lilian Besson**.
I'm a French student at *ÉNS de Cachan*, 
currently I'm doing two MSc Mathematics and computer science (CS).
 
Feel free to *contact* me :
 0. either with this web page `~besson <http://perso.crans.org/~besson>`_;
 1. or via my *bitbucket* account `lbesson <https://bitbucket.org/lbesson>`_;
 2. or via *Facebook* `naereencorp.lbesson 
    <https://www.facebook.com/naereencorp.lbesson>`_;
 3. or via my *Google Site* site `naereencorp 
    <https://sites.google.com/site/naereencorp>`_;
 4. or via *email* `here (remove the [] and change DOT to . and AT to @ 
    <mailto:lilian DOT besson AT ens-cachan [DOT] fr>`_.


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Table of contents
-----------------

.. toctree::
   :maxdepth: 5
   
   ANSIColors
   TODO

------------------------------------------------------------------------------

.. _licence:

Extract of the license (*GPL v3*)
---------------------------------

For the complete license, see this `file <../../LICENSE>`_.

    ANSIColors & ANSIColors-balises
    Copyright (C) 2012-13 Lilian Besson

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see http://www.gnu.org/licenses/.

.. (c) Lilian Besson 2012-2013
