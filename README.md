ANSI Colors
===========

**WARNING** : this project is now *deprecated*, please see [the new version (ansicolortags.py)](https://bitbucket.org/lbesson/ansicolortags.py) : it is cleaner, and supports both Python 2 and 3 (and [its doc is on Read the Docs !](http://ansicolortags.readthedocs.io/))
**WARNING**

----

A Python script and module to simply use ANSI Colors in a terminal.

![Screenshot of the doc, colored with the script](http://perso.crans.org/~besson/publis/ansi-colors/example1.png "Screenshot of the doc, colored with the script")

----

### Author:
Lilian Besson.

### Language:
Python v2.7+ (but *not* v3).
A P3K compatible version is in progress !

This project is now hosted on [Pypi module repository](<https://pypi.python.org/pypi/ANSIColors-balises> "Pypi !").

Documentation
-------------

The complete documentation of the module is available, see [here on pythonhosted.org](<http://pythonhosted.org/ANSIColors-balises/> "on-line").

A short documentation is also available [here on pydoc.net](http://pydoc.net/Python/ANSIColors-balises/1.9.9.public/#description).
 
**All the details (installation, options, etc) are in the doc**. Anyway, here are somes infos.

----

Installation
============

The project is just the main script **ANSIColors.py**.

How to install it
-----------------

 Download or copy it from this *git* repository, then launch ``python setup.py install``.
 More details can be found in the **INSTALL** file.

Dependencies
------------
 
The project is *entirely written in Python*, version *2.7.3*.

For more details about the **Python** language, see [the official site](<http://www.python.org> "Python power !").
Python 2.7.1 or higher is **required**.

Plateform(s)
------------

The project have been *developped* on *GNU/Linux* (Ubuntu 11.10).

#### Warning (Windows)
It also have been quickly tested on *Windows 7* **with the Cygwin environment** and Python 2.7.

#### Warning (Mac OS X)  
It shall also work on *Mac OS X*, but **not been tested**.
Any suggestion or returns is welcome !
   
About the project
=================

This project was part of my work realised for the MPRI 1-21 **net programming lesson**.
The MPRI is the **Parisian Master for Research in Computer Science** (*Master Parisien de Recherche en Informatique* in French).

About the doc
=============
 
The documentation is produced mainly with **Sphinx**, the Python's documentation generator.
 
I wrote a few scripts to help *Sphinx* to do what I wanted, and to generate a *PyDoc* version of the doc too.
 
Those scripts constitute now an independant and a powerful project.
It is hosted [here on my Google Site](https://sites.google.com/site/naereencorp/liste-des-projets/makepydoc "check this out too ;) !")


Contact me
----------

Feel free to contact me, either with a bitbucket message (my profile is [lbesson](https://bitbucket.org/lbesson/ "here")), or via an email at **lilian DOT besson AT ens-cachan DOT fr**.

License
-------

This project is released under the **GPLv3 license**, for more details, take a look at the LICENSE file in the source.
*Basically, that allow you to use all or part of the project for you own business.*
