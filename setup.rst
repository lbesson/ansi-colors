setup Module
============

.. automodule:: setup
    :members:
    :private-members:
    :special-members:
    :show-inheritance:

..
   This .rst file have been created using the Naereen CORP. script pytorst.sh,
   for the module 'setup.py', with the script '/usr/local/bin/pytorst.sh' in the directory '/home/lilian/Bureau/ansicolors'.
   Generated the mardi 19 mars 2013, 12:01:25 (UTC+0100), by 'lilian' on 'naereen-corp'.
   Feel free to contact us : Naereen Corp. (c) 12-01-2013
   https://sites.google.com/site/naereencorp/
   mailto:naereen-corporation[AT]laposte.net

