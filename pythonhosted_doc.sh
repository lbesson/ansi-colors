#!/bin/bash
#
#	This script is designed to be used with makePydoc, update__date__ and pylint.
#	The reference page for this software is : 
#	https://sites.google.com/site/naereencorp/liste-des-projets/makepydoc
#
#	__author__='Lilian BESSON'
#	__email__='lilian.besson@normale.fr'
#	__version__='1.3'
#	__date__='mer. 20/03/2013 at 11h:31m:10s '
#
# A simple script to automatize the generation of the zip archive
# to send to pythonhosted.org.

if [ -f doc_ANSIColors-balises.zip ]; then
	echo "The doc was already there, I'm deleting it ..."
	rm -f doc_ANSIColors-balises.zip
fi

cd .build/html
zip -r -9 -T -v -u ../../doc_ANSIColors-balises.zip ./
cd ../..

echo ""

echo "Now, go to [https://pypi.python.org/pypi?:action=pkg_edit&name=ANSIColors-balises], and upload (manually) the file doc_ANSIColors-balises.zip..."