============================
TODOs for ANSIColors package
============================

Important changes
-----------------

Those modifications are important 
(the goal is to brought back the retro
compatibility with Python 2.6).

By now, the script fails when it is used with P2.6 (or -).

.. versionadded:: 1.9.8

   But the module is now ok.
   I moved the :code:`import argparse` to the function 
   :pyfunction:`_parser_default`.


Secondary changes
------------------

1. look for an idea to provide a Windows version.
2. idem for MAC OS X.

**Or at least, test it** :
 1. if it is ok : ok,
 2. otherwise, explain why and say it in the main page (file `README.md`).

Idea
~~~~

Use :code:`sys.version` or :code:`sys.platform` 
to change the way the colours are implemented.

*(Again)* or at least, print an explanatory message 
in case the OS is not supported.

Python 3 support
----------------

Last time I tested, it was not OK :

 * problem when converting with `2to3` tool :
 
    * tab and space inconsistency(ies),
    * the :code:`except:` is durty,
    
 * so, find a way to :
 
    * correct those,
    * convert *easily* to P3k.

--------------------------------------------------------------------------

.. warning::

   This page is still in construction.

.. (c) Lilian Besson 2012-2013
